// Знайти всі параграфи на сторінці та встановити колір фону #ff0000

const paragraph = document.querySelectorAll('p');
paragraph.forEach(paragraph => {
   paragraph.style.background = "#ff0000";
});

// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль.
// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

const optionsList = document.getElementById("optionsList");
console.log(optionsList);
console.log(optionsList.parentElement);
console.log(optionsList.childNodes);

optionsList.childNodes.forEach(node => {
      console.log(node.nodeType);
    }
);

optionsList.childNodes.forEach(node => {
      console.log(node.nodeName);
    }
);

// Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph

const testParagraph = document.querySelector("#testParagraph").innerHTML = "This is a paragraph";

// Отримати елементи , вкладені в елемент із класом main-header і вивести їх у консоль.
// Кожному з елементів присвоїти новий клас nav-item.

const mainHeaderChilds = document.querySelector(".main-header").children;
console.log(mainHeaderChilds);
for(let item of mainHeaderChilds) {
    item.classList.add("nav-item");
}

// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

const sectionTitleAll = document.querySelectorAll(".section-title");
console.log(sectionTitleAll);
sectionTitleAll.forEach(title => {
    title.classList.remove("section-title");
});
console.log(sectionTitleAll);
